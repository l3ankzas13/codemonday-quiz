# Codemonday Quiz

### Clone Project
```
git clone git@gitlab.com:l3ankzas13/codemonday-quiz.git codemonday-quiz
cd codemonday-quiz
```

### Start Docker
```
docker-compose up -d
```

### Migrate Database
```
docker exec -it codemonday_app yarn migrate
```

### Log monitor
```
docker logs codemonday_app --follow
```

### API
```
http://localhost:3000
```

### API Documentation
```
http://localhost:3000/api-docs
```

