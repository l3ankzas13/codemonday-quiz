export default function validator(body, rules) {
  return Object.entries(body).reduce((data, [key, value]) => {
    const rule = rules[key]
    if (rule.required) {
      if (value) return { isValid: data.isValid !== false, errors: data.errors }
      return { isValid: false, errors: [...data.errors, { [key]: rule.error }] }
    }
    // todo: add new rule for validate
    return data
  }, { isValid: true, errors: [] })
}
