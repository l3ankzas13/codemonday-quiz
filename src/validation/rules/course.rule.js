export const createCourseValidationRules = {
  course_name: {
    required: true,
    error: 'Please input Course Name'
  },
  course_description: {
    required: true,
    error: 'Please input Course Description'
  },
  teacher_id: {
    required: true,
    error: 'Please input Teacher ID'
  },
}

export const findCourseValidationRules = {
  id: {
    required: true,
    error: 'Please send id'
  }
}
