export const findTeacherValidationRules = {
  id: {
    required: true,
    error: 'please send id'
  },
}

export const createTeacherValidationRules = {
  first_name: {
    required: true,
    error: 'Please input First Name'
  },
  last_name: {
    required: true,
    error: 'Please input Last Name'
  },
  age: {
    required: true,
    error: 'Please input Age'
  },
}

export const updateTeacherValidationRules = {
  id: {
    required: true,
    error: 'Please input id'
  },
  first_name: {
    required: true,
    error: 'Please input First Name'
  },
  last_name: {
    required: true,
    error: 'Please input Last Name'
  },
  age: {
    required: true,
    error: 'Please input Age'
  },
}