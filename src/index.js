import express from 'express';
import logger from 'morgan';
import swaggerUi from 'swagger-ui-express';
import YAML from 'yamljs';
import path from 'path';


import teacherRoute from './routes/teacher.route'
import courseRoute from './routes/course.route'

const swaggerPath = path.resolve(__dirname, './swaggers/swagger.yaml');
const swaggerDocument = YAML.load(swaggerPath);

const app = express();
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use('/teacher', teacherRoute);
app.use('/course', courseRoute);

app.get('/', (req, res) => {
  res.status(200).json({ message: `Hello, I'm Aekkawan Klapsan` })
});

app.listen(process.env.PORT, () => {
  console.log('server listening on port: ', process.env.PORT)
})

export default app;
