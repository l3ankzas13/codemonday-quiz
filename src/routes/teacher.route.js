import express from 'express';

import * as teacherController from '../controllers/teacher.controller'

const router = express.Router();

router.get('/', teacherController.getTeachers);
router.get('/:id', teacherController.getTeacher);
router.post('/', teacherController.createTeacher);
router.put('/', teacherController.updateTeacher);

export default router;
