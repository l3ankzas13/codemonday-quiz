import express from 'express';

import * as courseController from '../controllers/course.controller'

const router = express.Router();

router.get('/', courseController.getCourses);
router.get('/:id', courseController.getCourse);
router.post('/', courseController.createCourses);

export default router;
