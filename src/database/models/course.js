'use strict';

module.exports = (sequelize, DataTypes) => {
  const Course = sequelize.define('Course', {
    course_name: DataTypes.STRING,
    course_description: DataTypes.TEXT,
  })

  Course.associate = models => {
    Course.belongsTo(models.Teacher, {
      as: 'teacher',
      foreignKey: 'teacher_id',
      targetKey: 'id',
    });
  };

  return Course;
};