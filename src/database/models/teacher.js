'use strict';

module.exports = (sequelize, DataTypes) => {
  const Teacher = sequelize.define('Teacher', {
    first_name: DataTypes.STRING,
    last_name: DataTypes.STRING,
    age: DataTypes.NUMBER
  })

  Teacher.associate = models => {
    Teacher.hasMany(models.Course, {
      as: 'course',
      foreignKey: 'teacher_id',
      targetKey: 'id',
    });
  };

  return Teacher;
};