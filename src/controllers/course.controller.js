import models from '../database/models';
import * as validateRules from '../validation/rules/course.rule'
import validator from '../validation/validator'

export async function getCourses(req, res) {
  let message = 'Success';
  let status = 200;
  let errors = '';
  let data = ''

  try {
    const courses = await models.Course.findAll({ include: ['teacher'] })
    if (courses) data = courses;
    return;

  } catch (error) {
    console.log('error', error);
    status = 400;
    message = 'Bad request';
    errors = 'Bad request';
    return;

  } finally {
    res.status(status).json({ message, errors, data });
  }
}


export async function getCourse(req, res) {
  let message = 'Success';
  let status = 200;
  let errors = '';
  let data = '';

  try {
    const validated = validator(req.params, validateRules.findCourseValidationRules)
    if (!validated.isValid) {
      message = 'Bad request';
      status = 400;
      errors = validated.errors;
      return;
    }

    const { id = '' } = req?.params
    const courses = await models.Course.findOne({ include: ['teacher'], where: { id } })
    if (courses) data = courses;
    return;

  } catch (error) {
    console.log('error', error);
    status = 400;
    message = 'Bad request';
    errors = 'Bad request';
    return;

  } finally {
    res.status(status).json({ message, errors, data });
  }
}

export async function createCourses(req, res) {
  let message = 'Success'
  let errors = ''
  let status = 200
  let data = ''

  try {
    const validated = validator(req.body, validateRules.createCourseValidationRules)
    if (!validated.isValid) {
      message = 'Bad request';
      status = 400;
      errors = validated.errors;
      return;
    }

    const { course_name, course_description, teacher_id } = req?.body
    const course = await models.Course.create({
      course_name,
      course_description,
      teacher_id
    }, {
      returning: true,
      plain: true
    })
    if (course) {
      message = 'Created success'
      data = course
    }
    return;

  } catch (error) {
    console.log('error', error)
    status = 400
    message = 'Bad request'
    return;
  } finally {
    res.status(status).json({ message, errors, data });
  }
}

