import models from '../database/models';
import * as validateRules from '../validation/rules/teacher.rule'
import validator from '../validation/validator'

export async function getTeachers(req, res) {
  let message = 'success';
  let status = 200;
  let errors = '';
  let data = '';

  try {
    const teachers = await models.Teacher.findAll({ include: ['course'] });
    if (teachers) data = teachers;
  } catch (error) {
    console.log('error', error);
    message = 'error';
    status = 400;
  } finally {
    res.status(status).json({ message, errors, data });
  }
}

export async function getTeacher(req, res) {
  let message = 'success';
  let errors = '';
  let status = 200;
  let data = '';
  try {
    const validated = validator(req?.params, validateRules.findTeacherValidationRules)
    if (!validated.isValid) {
      message = 'error';
      status = 400;
      errors = validated.errors;
      return;
    }
    const { id = '' } = req?.params
    const teachers = await models.Teacher.findOne({ include: ['course'], where: { id } })
    if (teachers) data = teachers
  } catch (error) {
    console.log('error', error);
    message = 'error';
    status = 400;
  } finally {
    res.status(status).json({ message, errors, data });
  }
}

export async function createTeacher(req, res) {
  let message = 'Created success';
  let status = 200;
  let errors = '';
  let data = '';

  try {
    const validated = validator(req.body, validateRules.createTeacherValidationRules)
    if (!validated.isValid) {
      message = 'error';
      status = 400;
      errors = validated.errors;
      return;
    }

    const { first_name, last_name, age } = req?.body
    const teacher = await models.Teacher.create({
      first_name,
      last_name,
      age
    }, {
      returning: true,
      plain: true
    })
    if (teacher) {
      console.log('teacher', teacher)
      data = teacher
      message = 'Created success'
    }
    return;

  } catch (error) {
    console.log('error', error)
    status = 400
    return;

  } finally {
    res.status(status).json({ message, errors, data });
  }
}

export async function updateTeacher(req, res) {
  let message = 'Updated success'
  let errors = ''
  let status = 200
  let data = ''

  try {
    const validated = validator(req.body, validateRules.updateTeacherValidationRules)
    if (!validated.isValid) {
      message = 'error'
      status = 400
      errors = validated.errors
      return;
    }

    const {
      id = '',
      first_name = '',
      last_name = '',
      age = ''
    } = req?.body

    const teacher = await models.Teacher.update({
      first_name,
      last_name,
      age
    }, {
      where: { id },
      returning: true,
      plain: true
    })

    if (teacher && teacher.length > 1) {
      message = 'Updated success'
      data = teacher[1]
    } else {
      message = 'Updated fail'
    }
    return;

  } catch (error) {
    console.log('error', error)
    message = 'Updated fail'
    status = 400
    return;
  } finally {
    res.status(status).json({ message, errors, data });
  }
}